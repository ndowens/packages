# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: 
pkgname=parted
pkgver=3.2
pkgrel=2
pkgdesc="Utility to create, destroy, resize, check and copy partitions"
url="https://www.gnu.org/software/parted/parted.html"
arch="all"
license="GPL-3.0+"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
depends=""
makedepends="ncurses-dev lvm2-dev bash util-linux-dev autoconf automake"
checkdepends="check-dev python3"
source="https://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.xz
	bsd-endian.patch
	disable-two-tests.patch
	fix-includes.patch
	posix-shell.patch
	sysmacros.patch
	tests-call-name-correctly.patch
	tests-python3.patch
	"

prepare() {
	default_prepare
	autoreconf
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-debug \
		--enable-static \
		--enable-shared \
		--enable-device-mapper \
		--without-readline
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	rm -rf "$pkgdir"/usr/lib/charset.alias
	rmdir -p "$pkgdir"/usr/lib 2>/dev/null || true
}

sha512sums="4e37dbdd6f5032c9ebfec43704f6882379597d038167b9c4d04053efa083c68a705196713864451fa9e11d32777e16c68982877945c5efd9ea5f8252cb20e1c4  parted-3.2.tar.xz
d3c16548cac315dad100c992c0b8446d1f7978cb45e867c69e746b25781c06802d3b4aab7d9346e44e68f61bb07e41ff65ef75d0dc6613f6fa8472b2e67a76a7  bsd-endian.patch
b49eb0211c405a4ef69e1bb0079621c22fec4adf0309f0a58b5d881540913de01d05ccd3eb521f35c3cf6f4eb8aa0f4c5270ce3dea47270a5c3fe8fae635c6f0  disable-two-tests.patch
55ee63c218d1867c0f2c596e7c3eec5c42af160181456cc551fe3d432eabed0ac2dd3a3955ff0c375f76aeec8071e7f55a32834b87a0d39b8ef30361f671bfdd  fix-includes.patch
6710a92cfb0a455de5e0c58f50c3c081c564307463c448fdc62521b868cb08fc10b2c558b284ec2e4ebbea7e940bda4b9e8e7aa7db58aec1b856451df9c54650  posix-shell.patch
5d2e8f22b6cd5bdd3289996848279a945ca09acd2862e82283bb769c2e4d61a24a31e1793d81385e8f3f1f4d48417e2308c5ea39dac47e832666363dde044ba7  sysmacros.patch
8bd86d2b0401566e7757c43d849b7f913cc4ec1bf50d5641dc72d7e278ca38db2ac746cd8dcc756b245021ea1f9738875b6a831f05185b9217d3f1c287944748  tests-call-name-correctly.patch
7486f98d535380a9e6598b9ac6153564319d5effa25456dc393cf3540ea47ac5b462be79cbd7d8efbd1fc2d2ef240a00873a2e2b138d4b4b0bb1494893de1eac  tests-python3.patch"
