# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=skalibs
pkgver=2.9.2.1
pkgrel=0
pkgdesc="A set of general-purpose C programming libraries for skarnet.org software"
url="https://skarnet.org/software/skalibs/"
arch="all"
options="!check"  # No test suite.
license="ISC"
subpackages="$pkgname-libs $pkgname-dev $pkgname-libs-dev:libsdev $pkgname-doc"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz"

build() {
	./configure \
		--enable-shared \
		--enable-static \
		--libdir=/usr/lib
	make
}

package() {
	make DESTDIR="$pkgdir" install
}


libs() {
	pkgdesc="$pkgdesc (shared libraries)"
	depends=""
	mkdir -p "$subpkgdir/lib"
	mv "$pkgdir"/lib/*.so.* "$subpkgdir/lib/"
}


dev() {
	pkgdesc="$pkgdesc (development files)"
	depends=""
	install_if="dev $pkgname=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir"
	mv "$pkgdir/usr" "$subpkgdir/"
}


libsdev() {
	pkgdesc="$pkgdesc (development files for dynamic linking)" 
	depends="$pkgname-dev"
	mkdir -p "$subpkgdir/lib"
	mv "$pkgdir"/lib/*.so "$subpkgdir/lib/"
}


doc() {
	pkgdesc="$pkgdesc (documentation)" 
	depends=""
	install_if="docs $pkgname=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir/usr/share/doc"
	cp -a "$builddir/doc" "$subpkgdir/usr/share/doc/$pkgname"
}

sha512sums="bd507faf30f0564fb8d3e5f86de04ab4b39961dab2497ba9f4a81980a57f7ff68cad366b9847b25d555b5b9263460d13f096e1774d080d1d286979f2e7324c54  skalibs-2.9.2.1.tar.gz"
