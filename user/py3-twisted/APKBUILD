# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=py3-twisted
_pkgname=Twisted
_p="${_pkgname#?}"
_p="${_pkgname%"$_p"}"
pkgver=19.10.0
pkgrel=0
pkgdesc="Asynchronous networking framework written in Python"
url="https://twistedmatrix.com/"
arch="all"
# FIXME: constantly, automat, hyperlink, and PyHamcrest
# also need to be packaged and added to depends=
options="net"
license="MIT"
depends="python3 py3-attrs py3-incremental py3-zope-interface"
makedepends="python3-dev"
subpackages=""
source="https://files.pythonhosted.org/packages/source/$_p/$_pkgname/$_pkgname-$pkgver.tar.bz2
	CVE-2020-10108-and-2020-10109.patch
	"
builddir="$srcdir/Twisted-$pkgver"

# secfixes: twisted
#   19.10.0-r0:
#     - CVE-2019-9512
#     - CVE-2019-9514
#     - CVE-2019-9515
#     - CVE-2020-10108
#     - CVE-2020-10109

build() {
	python3 setup.py build
}

check() {
	python3 setup.py test
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="de8d7fd0b2081cebeff68b060c8469377011648bc563a94a993d3530fb007ed42c3a54925c9a10c465ee7a3065cc9108ace12d10d358223fab13494becb9ac4b  Twisted-19.10.0.tar.bz2
dcc22af0a72bce5cca3c9f5ee739b17f14275ac8e776c658743746239c793ad9585fb0333203945912fd4994bd001b7244905562c4389304a707a2a4430faa40  CVE-2020-10108-and-2020-10109.patch"
