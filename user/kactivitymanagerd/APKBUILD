# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=kactivitymanagerd
pkgver=5.18.5
pkgrel=0
pkgdesc="Service to manage KDE Plasma activities"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0-only"
depends="qt5-qtbase-sqlite"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kdbusaddons-dev ki18n-dev
	boost-dev python3 kconfig-dev kcoreaddons-dev kwindowsystem-dev kio-dev
	kglobalaccel-dev kxmlgui-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/kactivitymanagerd-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild .
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

sha512sums="2ee59f78cc948fe11577373ea69f895efcaa7d4f239362d3e060382c3be0f67511a0baa64a404c1e0e53d5777775afba60f49e58476c70dcdef6c63358ca4dbc  kactivitymanagerd-5.18.5.tar.xz"
