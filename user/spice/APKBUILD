# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=spice
pkgver=0.14.3
pkgrel=0
pkgdesc="Solution for seamless access to virtual machines"
url="https://www.spice-space.org/"
arch="all"
options="!check"  # Tests only pass on 64-bit architectures.
license="LGPL-2.1+"
depends="gst-plugins-base"
makedepends="$depends_dev openssl-dev zlib-dev libjpeg-turbo-dev cyrus-sasl-dev
	opus-dev lz4-dev gstreamer-dev gst-plugins-base-dev glib-dev orc-dev
	python3 spice-protocol pixman-dev gstreamer-tools"
subpackages="$pkgname-dev"
source="https://www.spice-space.org/download/releases/spice-server/spice-$pkgver.tar.bz2"

# secfixes:
#   0.14.1-r0:
#     - CVE-2018-10873

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-celt051
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="9ecdc455ff25c71ac1fe6c576654b51efbfb860110bd6828065d23f7462d5c5cac772074d1a40f033386258d970b77275b2007bcfdffb23fdff2137154ea46e4  spice-0.14.3.tar.bz2"
