# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer: 
pkgname=pciutils
pkgver=3.7.0
pkgrel=0
pkgdesc="PCI bus configuration space access library and tools"
url="http://mj.ucw.cz/pciutils.html"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0-only"
depends="hwids-pci"
makedepends="eudev-dev kmod-dev linux-headers cmd:which"
subpackages="$pkgname-doc $pkgname-dev $pkgname-libs"
source="https://www.kernel.org/pub/software/utils/$pkgname/$pkgname-$pkgver.tar.gz
	fix-linking-pci-malloc-Makefile.patch
	pread.patch
	"

build() {
	sed -i -e "106s/^/\#/" Makefile
	make OPT="$CFLAGS" ZLIB=no \
		SHARED=yes \
		PREFIX=/usr \
		SHAREDIR=/usr/share/hwdata \
		MANDIR=/usr/share/man \
		all
}

package() {
	make PREFIX="$pkgdir"/usr \
		SHARED=yes \
		SHAREDIR="$pkgdir"/usr/share/hwdata \
		MANDIR="$pkgdir"/usr/share/man \
		install install-lib

	mkdir "$pkgdir"/usr/bin/
	mv "$pkgdir"/usr/sbin/lspci "$pkgdir"/usr/bin/lspci
	mkdir "$pkgdir"/usr/share/man/man1
	mv "$pkgdir"/usr/share/man/man8/lspci.8 \
		"$pkgdir"/usr/share/man/man1/lspci.1

	rm "$pkgdir"/usr/sbin/update-pciids
	rm "$pkgdir"/usr/share/man/man8/update-pciids.8
	rm -r "$pkgdir"/usr/share/hwdata
}

sha512sums="37c01619264d299ac1e367a022637d60ab2bf6e5533fe9d37b2d37fdc5070563b78bfedc200b6bcb9192ce43fdec09ff350080c3e3ba504b90766d004398efc5  pciutils-3.7.0.tar.gz
520b39602078e4325d7dac2d460547b360f7b52c668d88cf3d776c59246c8cfcb537b7b4f50575da9d2fcea1e207b3e99626ce4f23df890d2565b7dac1db2d94  fix-linking-pci-malloc-Makefile.patch
aa1d3272500180256929b39a5fc4ea38ddb6a9fad090d732832ded9cc396303cf56efc91cfdeb76edbcfefc9a7c8eb058562c3c2b09a9090a59d3e68c27cec62  pread.patch"
