# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=plasma-desktop
pkgver=5.18.5
pkgrel=0
pkgdesc="Modern, functional, integrated libre desktop environment"
url="https://www.kde.org/plasma-desktop"
arch="all"
options="!check"  # Test suite fails due to keyboard layouts
license="GPL-2.0 LGPL-2.1"
# theme/visual dependencies
depends="breeze breeze-icons kirigami2 oxygen qt5-qtquickcontrols2
	qqc2-desktop-style ttf-liberation"
# shell/QML dependencies
depends="$depends kde-cli-tools kded kwin qt5-qtgraphicaleffects setxkbmap"
# Good Ideas™ to have
depends="$depends elogind kdeclarative polkit-kde-agent-1"
makedepends="cmake qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtx11extras-dev
	qt5-qtsvg-dev kdelibs4support-dev xf86-input-synaptics-dev
	kauth-dev plasma-framework-dev kdoctools-dev ki18n-dev libkworkspace-dev
	kcmutils-dev knewstuff-dev knotifications-dev knotifyconfig-dev
	attica-dev kwallet-dev krunner-dev kglobalaccel-dev kdeclarative-dev
	kpeople-dev kdbusaddons-dev kactivities-dev kactivities-stats-dev
	kconfig-dev kitemmodels-dev plasma-workspace-dev xf86-input-evdev-dev
	libxkbcommon-dev pulseaudio-dev libcanberra-dev libxkbfile-dev
	xorg-server-dev kwin-dev kscreenlocker-dev baloo-dev xcb-util-image-dev
	xf86-input-libinput-dev breeze-dev xkeyboard-config extra-cmake-modules
	kirigami2-dev libksysguard-dev qt5-qtquickcontrols2-dev
	qqc2-desktop-style-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-desktop-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_TESTING=OFF \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="14f86d1158f976e8f0fbf80fb7f5bea74393febc35592bd95806548883b3bc2e77ee55cc300119275ad322f6b8bcfbed84bb6b78a3a19868ce4695a1fb57a029  plasma-desktop-5.18.5.tar.xz"
