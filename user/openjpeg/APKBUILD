# Contributor: William Pitcock <nenolod@dereferenced.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=openjpeg
pkgver=2.3.1
pkgrel=3
pkgdesc="Open-source implementation of JPEG 2000 image codec"
url="http://www.openjpeg.org/"
arch="all"
options="!check"  # No test suite.
license="BSD-2-Clause-NetBSD"
depends=""
depends_dev="$pkgname-tools"
makedepends="libpng-dev tiff-dev lcms2-dev doxygen cmake"
subpackages="$pkgname-dev $pkgname-tools"
source="$pkgname-$pkgver.tar.gz::https://github.com/uclouvain/openjpeg/archive/v$pkgver.tar.gz
	CVE-2019-12973.patch
	CVE-2020-6851.patch
	CVE-2020-8112.patch
	"

# secfixes:
#   2.3.1-r3:
#     - CVE-2020-6851
#     - CVE-2020-8112
#   2.3.1-r2:
#     - CVE-2019-12973
#   2.3.0-r0:
#     - CVE-2017-14039
#   2.2.0-r2:
#     - CVE-2017-14040
#     - CVE-2017-14041
#     - CVE-2017-14151
#     - CVE-2017-14152
#     - CVE-2017-14164
#   2.2.0-r1:
#     - CVE-2017-12982
#   2.1.2-r1:
#     - CVE-2016-9580
#     - CVE-2016-9581

build() {
	cmake . \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DOPENJPEG_INSTALL_LIB_DIR=lib \
		-DOPENJPEG_INSTALL_PACKAGE_DIR=lib/cmake/$pkgname-${pkgver%.*}
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

tools() {
	pkgdesc="$pkgdesc (tools)"
	mkdir -p "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="339fbc899bddf2393d214df71ed5d6070a3a76b933b1e75576c8a0ae9dfcc4adec40bdc544f599e4b8d0bc173e4e9e7352408497b5b3c9356985605830c26c03  openjpeg-2.3.1.tar.gz
472deba1d521553f9c7af805ba3d0c4fc31564fd36e37c598646f468b7d05bf5f81d2320fd6fadf8c0e3344ebce7bc0d04cece55a1b3cec2ef693a6e65bd2516  CVE-2019-12973.patch
c8ffc926d91392b38250fd4e00fff5f93fbf5e17487d0e4a0184c9bd191aa2233c5c5dcf097dd62824714097bba2d8cc865bed31193d1a072aa954f216011297  CVE-2020-6851.patch
9659e04087e0d80bf53555e9807aae59205adef2d49d7a49e05bf250c484a2e92132d471ec6076e57ca69b5ce98fd81462a6a8c01205ca7096781eec06e401cc  CVE-2020-8112.patch"
