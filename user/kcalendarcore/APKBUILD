# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcalendarcore
pkgver=5.74.0
pkgrel=0
pkgdesc="Library for managing a calendar of events"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev attica-dev kparts-dev"
checkdepends="tzdata"
makedepends="$depends_dev cmake extra-cmake-modules libical-dev
	doxygen qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc"
replaces="kcalcore"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcalendarcore-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	TZ=UTC CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(Compat-libical3|testicaltimezones)'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="f6b48283d5433e171a1961abcbf47422fb8ce1e6412d0a83de20c16b2e81e065e16c1525440e1a29791b31ec74fbe672878cd554fceab703cc0c9c45c23524af  kcalendarcore-5.74.0.tar.xz"
