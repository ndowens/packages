# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libktorrent
pkgver=2.2.0
pkgrel=0
pkgdesc="Torrent handling library for KDE"
url="https://www.kde.org/"
arch="all"
options="!check"  # All tests require X11
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev karchive-dev kcrash-dev kio-dev solid-dev gmp-dev
	boost-dev qca-dev libgcrypt-dev"
makedepends="cmake extra-cmake-modules $depends_dev doxygen ki18n-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/ktorrent/5.2.0/libktorrent-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="2edcb3a230f4fa3a55a6b774c819f4964fe8f2f5447b723ba81cdb0c187dc0268b6e78566d710fa364abe2cc40cd618d1874ecb1637e872fbad0ea8c340abc1a  libktorrent-2.2.0.tar.xz"
