# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libvncserver
pkgver=0.9.13
pkgrel=0
pkgdesc="Library to make writing a vnc server easy"
url="https://libvnc.github.io/"
arch="all"
license="GPL-2.0+"
depends=""
depends_dev="libgcrypt-dev libjpeg-turbo-dev gnutls-dev libpng-dev
	libice-dev libx11-dev libxdamage-dev libxext-dev libxfixes-dev
	libxi-dev libxinerama-dev libxrandr-dev libxtst-dev"
makedepends="$depends_dev cmake"
subpackages="$pkgname-dev"
source="https://github.com/LibVNC/libvncserver/archive/LibVNCServer-$pkgver.tar.gz"
builddir="$srcdir"/libvncserver-LibVNCServer-$pkgver

# secfixes:
#   0.9.11-r0:
#     - CVE-2016-9941
#     - CVE-2016-9942
#   0.9.12-r0:
#     - CVE-2018-15127
#   0.9.12-r1:
#     - CVE-2019-15681
#     - CVE-2019-15690
#   0.9.13-r0:
#     - CVE-2019-20788
#     - CVE-2020-14401

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
	test/tjunittest
}

package() {
	make install DESTDIR="$pkgdir"
}

sha512sums="18b0a1698d32bbdbfe6f65f76130b2a95860e3cc76e8adb904269663698c7c0ae982f451fda1f25e5461f096045d40a89d9014258f439366d5b4feaa4999d643  LibVNCServer-0.9.13.tar.gz"
