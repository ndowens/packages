# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=baloo-widgets
pkgver=20.08.1
pkgrel=0
pkgdesc="Widgets that utilise the Baloo desktop indexing engine"
url="https://www.KDE.org/"
arch="all"
options="!check"  # Requires /etc/fstab to exist, and udisks2 to be running.
license="GPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev baloo-dev kio-dev"
makedepends="$depends_dev cmake extra-cmake-modules kconfig-dev
	kfilemetadata-dev ki18n-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/baloo-widgets-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="e64719df49742852fadd8ca7e18bb725c5e7259f9dee2b96f972e334eb3cf846b45ac5f9fb91a425e9354a9a2476d1bdb89761a34804d17c1f68e8958db9af5a  baloo-widgets-20.08.1.tar.xz"
