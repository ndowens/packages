# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=plasma-integration
pkgver=5.18.5
pkgrel=0
pkgdesc="Qt platform theme plugin for Plasma"
url="https://www.kde.org/"
arch="all"
options="!check"  # Times out, requires X11 desktop.
license="(LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.1-only AND ((LGPL-2.1-only WITH Nokia-Qt-exception-1.1) OR (GPL-3.0-only WITH Nokia-Qt-exception-1.1))"
depends="breeze ttf-noto"
makedepends="cmake extra-cmake-modules python3 qt5-qtbase-dev libxcursor-dev
	qt5-qtx11extras-dev kconfig-dev kconfigwidgets-dev ki18n-dev kio-dev
	kiconthemes-dev knotifications-dev kwayland-dev	kwidgetsaddons-dev
	kwindowsystem-dev breeze-dev qt5-qtquickcontrols2-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-integration-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DCMAKE_DISABLE_FIND_PACKAGE_FontNotoSans=true \
		-DCMAKE_DISABLE_FIND_PACKAGE_FontHack=true \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="d217c49555ee17e3b18d006368ec7aa4d8585c05070bcf3a3c7e471b1c7dc830080d0405209979d8a27c7c2fc2adcd3d248e595f43f0b4a2928683f93c6d4b48  plasma-integration-5.18.5.tar.xz"
