# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=bluez-qt
pkgver=5.74.0
pkgrel=0
pkgdesc="Qt integration with BlueZ"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires running D-Bus server.
license="LGPL-2.1-only OR LGPL-3.0-only"
depends=""
makedepends="cmake doxygen extra-cmake-modules qt5-qtbase-dev
	qt5-qtdeclarative-dev qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/bluez-qt-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		-DBUILD_TESTING:BOOL=OFF \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="dfd13e5610233453596e633766c0da30d4f6ba219af32249ce502f5f6a64e9315d26408a5bfd6fc2f51043df531c73127d9c431ff51a0159a043aa681bd743c6  bluez-qt-5.74.0.tar.xz"
