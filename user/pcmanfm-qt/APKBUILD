# Contributor: Kiyoshi Aman <adelie@aerdan.vulpine.house>
# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=pcmanfm-qt
pkgver=0.15.1
pkgrel=0
pkgdesc="File manager and desktop icon manager for LXQt"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0+"
depends=""
makedepends="cmake extra-cmake-modules lxqt-build-tools>=0.7.0
	liblxqt-dev>=${pkgver%.*}.0 libfm-qt-dev>=${pkgver%.*}.0
	qt5-qtx11extras-dev qt5-qttools-dev kwindowsystem-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxqt/pcmanfm-qt/releases/download/$pkgver/pcmanfm-qt-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="db0d3cc683b5c94cabab5a8c68b3ae8bf5938735b34989a420692e933bb54d53ebb15da9af4a59039bfd101de0cc00c521c08207e81bfad9d80315e2d94bc559  pcmanfm-qt-0.15.1.tar.xz"
