# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=i3wm
pkgver=4.18.2
pkgrel=0
pkgdesc="Improved dynamic tiling window manager"
url="https://i3wm.org"
arch="all"
options="!check"  # The test suite requires X
license="BSD-3-Clause"
depends=""
checkdepends="perl-x11-xcb perl-anyevent perl-json-xs perl-ipc-run
	perl-inline-c perl-dev libxcb-dev xcb-util-dev xorg-server-xephyr"
makedepends="bison flex libxcb-dev xcb-util-cursor-dev xcb-util-keysyms-dev
	xcb-util-wm-dev libev-dev pango-dev cairo-dev yajl-dev
	startup-notification-dev pcre-dev libxkbcommon-dev xcb-util-xrm-dev"
subpackages="$pkgname-doc"
source="https://i3wm.org/downloads/i3-$pkgver.tar.bz2
	i3wm-musl-glob-tilde.patch
	i3wm-test-fix-off_t.patch
	i3wm-test-disable-branch-check.patch"
builddir="$srcdir/i3-$pkgver"

build() {
	./configure \
		--prefix=/usr \
		--disable-builddir \
		--sysconfdir=/etc
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir/" install

	install -d "$pkgdir/usr/share/man/man1"
	install -m644 man/*.1 "$pkgdir"/usr/share/man/man1/
}

sha512sums="997c7cdf32f95ba25cfcd483a09f7e37e5219e4501baefa5c1865ddffd206bc9725211e4082663fab4174dc947b663ff454d343a7e30340ac3ea451f999417fc  i3-4.18.2.tar.bz2
6378e3619076c03345b4faa1f9d54cab2e7173068bc4d5f2f2894af9cc0e5792fe45ce95cb06328f5040f0ba6d43f3e49c523968732ac2d2046b698042338caa  i3wm-musl-glob-tilde.patch
77224b994397b2e2487ae28dfd5781b3630654191813eb3c685f05ebf446e65c36e53a665ff3cc8323ea67e87f7cf977044025dade0a6ed22cbd84f0e6b4cbc7  i3wm-test-fix-off_t.patch
a80384965dff62c51ce77e2baa3cf1b0b6db1df68994ce98383f96554bd296b4b59527fb5b1cb24b08c123699e294ba9b3baaa52afe88d87e7a76f0629194b1f  i3wm-test-disable-branch-check.patch"
