# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=knewstuff
pkgver=5.74.0
pkgrel=0
pkgdesc="Framework for discovering and downloading plugins, themes, and more"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends="kirigami2"
depends_dev="qt5-qtbase-dev attica-dev kconfig-dev kservice-dev kxmlgui-dev
	openssl-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev karchive-dev kcompletion-dev kcoreaddons-dev kio-dev
	ki18n-dev kiconthemes-dev kirigami2-dev kitemviews-dev kpackage-dev
	ktextwidgets-dev kwidgetsaddons-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/knewstuff-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# kmoretoolstest requires X11.
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E 'kmoretoolstest'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="038af364cf048035bc2d1a505e4f4c8318fd90efbfdd9c2fdffd9c22b1196a29b0d153643e838a98d472c19094e994a55983f84c63ef50f47574013a8b9f697a  knewstuff-5.74.0.tar.xz"
