# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=akonadi-search
pkgver=20.08.1
pkgrel=0
pkgdesc="Search functionality for Akonadi"
url="https://kde.org/"
arch="all"
license="LGPL-2.1+ AND (LGPL-2.1-only OR LGPL-3.0-only) AND GPL-2.0-only AND GPL-2.0+"
depends=""
makedepends="qt5-qtbase-dev cmake extra-cmake-modules kcmutils-dev kconfig-dev
	kcrash-dev kdbusaddons-dev ki18n-dev kio-dev krunner-dev akonadi-dev
	akonadi-mime-dev boost-dev kcalendarcore-dev kcontacts-dev kmime-dev
	xapian-core-dev kauth-dev kcodecs-dev kcoreaddons-dev kitemmodels-dev
	kjobwidgets-dev kpackage-dev kservice-dev solid-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-search-$pkgver.tar.xz
	lts.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	# sqlite backend requires D-Bus server.
	QT_QPA_PLATFORM=offscreen CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E 'akonadi-sqlite*'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="28e0235af1da68fac47b4535b0c5627d42de472d7d6238928a244dd729844f470c16c66c4092d25a33a373d790da8975c8b4d6fe429db209657003b50a39d7d3  akonadi-search-20.08.1.tar.xz
44245d992e5de6ebaa339c305e0f580f7ae83f605bc637180c9863421095feee9700d3ca6bd683a1360f90a727680a104737b5d985f8ec62a38c9aa7131d1b47  lts.patch"
