# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=akonadi-contacts
pkgver=20.08.1
pkgrel=0
pkgdesc="Library for integrating contact lists with Akonadi"
url="https://www.kde.org/"
arch="all"
options="!check"  # Tests require X11
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev akonadi-dev kcontacts-dev kcoreaddons-dev
	kservice-dev"
makedepends="$depends_dev cmake extra-cmake-modules akonadi-mime-dev kio-dev
	kmime-dev prison-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-contacts-$pkgver.tar.xz
	lts.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="5d7a481b60cc7d6068e4bd12fd22e695f43652e3ebe4e03e5512c5901240193408a2e72227de91f00c76833b957d7c9b26c62a4fe16615028229b46d40360b4b  akonadi-contacts-20.08.1.tar.xz
06ba7fb686e41eda820f98045832b80fd894e81c0b4a3edb50fbfbeb2d14a2e27e7536ca6729ad445326cd253c14ffa06653d73f7eaefa61ce9346b642792b73  lts.patch"
